## Welcome to YOUR orientation issue!

Welcome to Beautiful Canoe :canoe: :tada:

If you have any questions along the way, please feel free to ask on [Slack](https://beautifulcanoe.slack.com/), our chat communication tool, in the `#general` channel.

If you have any suggestions about orientation, please create a merge request in [this repository](https://gitlab.com/beautifulcanoe/peopleops/orientation-templates) and assign it to @snim2 to make this documentation better for the next new team member (you will learn about how to create merge requests as you progress through this issue).

This issue is divided into various tasks, you should focus on the **Developer** tasks.
If you can't move forward with your own tasks because someone else hasn't checked tasks that they are responsible for, please remind them by @ mentioning them in the comments for this issue.

At the end of your first thirty days, please complete our [orientation satisfaction survey](https://forms.gle/JhvNiz4yjkfT6o19A) and click the 'Close Issue' when all of your tasks have been completed.

---

## Contents

[[_TOC_]]

---

## Before starting at Beautiful Canoe

1. [ ] CEO: Check that the developer has a suitable laptop, or that we can provide one.
1. [ ] CEO: Check "Right to Work" check form is completed and returned to HR.
1. [ ] CEO: Complete Casual Worker paperwork and is signed by Budget Holder & Finance Manager.
1. [ ] CEO: Add developer to the shared Beautiful Canoe calendar (Summer Fellows only).
1. [ ] CTO: Create a confidential orientation issue in the [orientation](https://gitlab.com/beautifulcanoe/peopleops/orientation) project.

## First day tasks at Beautiful Canoe

### CEO tasks

1. [ ] CEO: Add developer to BCHQ access list.
1. [ ] CEO: Capture Developer Individual Expectations (Write a letter to themselves as if at the end of the fellowship)
1. [ ] CEO: Run (or arrange) Designed Alliance.
1. [ ] CEO: Run (or arrange) initial coaching sessions.

### CTO tasks

1. [ ] CTO: Add developer to `developers` group on GitLab.
1. [ ] CTO: Add developer to their client project repositories group on GitLab.

### Developer tasks

1. [ ] Developer: Provide Right To Work documentation to HR.
1. [ ] Developer: Obtain ID card from ITS.
1. [ ] Developer: Get a [GitLab](https://gitlab.com/) account and let the CTO have your username.
1. [ ] Developer: Fill out your GitLab profile, including adding a photo.
1. [ ] Developer: Join the company [Slack workspace](https://beautifulcanoe.slack.com/).
1. [ ] Developer: Fill out Slack profile, including adding a photo.
1. [ ] Developer: Join the `xxx-project` and `xxx-gitlab` channels for the relevant client project.

## First week tasks at Beautiful Canoe

### CEO tasks

1. [ ] CEO: Add the new developer to Trello boards for their client project.
1. [ ] CEO: Schedule an initial project meeting with the relevant client.

### CTO tasks

1. [ ] CTO: Run (or arrange) a training session on our development process and `git`.
1. [ ] CTO: Run (or arrange) a first week retrospective, on **Friday**.
1. [ ] CTO: Raise an issue and MR in the [company website repository](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com) to bump the company statistics.

### Developer tasks

1. [ ] Developer: Familiarise yourself with our [attitudes and values](https://beautifulcanoe.com/about.html).
1. [ ] Developer: Work through the [first day HOWTO](https://docs.beautifulcanoe.com/start-here/howto-first-day.html).
1. [ ] Developer: Work through the [first week HOWTO](https://docs.beautifulcanoe.com/start-here/howto-first-week.html).
1. [ ] Developer: Configure Git with our [recommended settings](https://docs.beautifulcanoe.com/git-howtos/howto-configure-git.html) and [commit template](https://docs.beautifulcanoe.com/git-howtos/howto-configure-git.html#adding-a-default-commit-message-template)
1. [ ] [Install `mdl`](https://docs.beautifulcanoe.com/start-here/howto-first-week.html#install-mdl)
1. [ ] Developer: Read through our development process documentation:
    1. [ ] [How to get code merged](https://docs.beautifulcanoe.com/start-here/howto-get-code-merged.html)
    1. [ ] [How to sprint](https://docs.beautifulcanoe.com/start-here/howto-sprint.html)
    1. [ ] [How to use labels](https://docs.beautifulcanoe.com/start-here/howto-use-labels.html)
    1. [ ] [How to review code changes](https://docs.beautifulcanoe.com/start-here/howto-review-changes.html)
    1. [ ] [How to work remotely](https://docs.beautifulcanoe.com/start-here/howto-work-remotely.html)
1. [ ] Developer: Raise MR to add self to the [company website](https://beautifulcanoe.com/).
1. [ ] Summer Fellow: Raise an MR to add self to the [list of Summer Fellows](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/-/blob/main/docs/start-here/bc-summer-fellows.md) in the [docs.beautifulcanoe.com](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/) repository.
1. [ ] Developer: Set up a suitable IDE and development environment.
1. [ ] Developer: Look for issues in your client project labels ~"Good First Issue" and raise an MR for at least one of them:
    1. ~"Good First Issue" [tickets in `beautifulcanoe`](https://gitlab.com/groups/beautifulcanoe/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Good%20First%20Issue).
    1. ~"Good First Issue" [tickets in `feed-quest`](https://gitlab.com/groups/feed-quest/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Good%20First%20Issue&first_page_size=20).
    1. ~"Good First Issue" [tickets in `traffic3d`](https://gitlab.com/groups/traffic3d/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Good%20First%20Issue).
1. [ ] Make sure you can access [the Aston VPN](https://docs.beautifulcanoe.com/start-here/howto-use-the-aston-vpn.html) - note that this will require you to have an Aston email address for single sign-on.
1. [ ] Developer: On **Friday** complete first week timesheets.
1. [ ] Developer: On **Friday** participate in first week retrospective.

### Skills review meeting

Towards the end of the week, the new developer should have a one-to-one call with either the CTO, Technical Director or a senior developer, to check that they are confident in the basic skills they will need during sprints.
During that call, they should demonstrate and check off the skills below.

I am confident that I can:

1. [ ] Read through a `CONTRIBUTING.md` document and set up a new project.
1. [ ] Set my text editor / IDE to delete trailing whitespace and use 4-space indentation.
1. [ ] Create a new issue, using one of the standard Beautiful Canoe issue templates.
1. [ ] Create a new merge request, and correctly label both the MR and related issue.
1. [ ] Find out which issues are assigned to me.
1. [ ] Find out which merge requests are assigned to me (for review).
1. [ ] Set one of my merge requests for review.
1. [ ] Use the Git aliases (such as `tree-log`) and commit template in the standard configuration.
1. [ ] Commit my work using `git add -p` or `git add -i` to split my work into small, atomic commits.
1. [ ] Write a useful commit message, using the standard template.
1. [ ] Bring a feature branch up to date with `develop` or `main`.
