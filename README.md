# Orientation templates

[![pipeline status](https://gitlab.com/beautifulcanoe/peopleops/orientation-templates/badges/main/pipeline.svg)](https://gitlab.com/beautifulcanoe/peopleops/peopleops-templates/-/commits/main)

This repository contains issue templates for orientating new Canoeists.

The issues itself are created in the [orientation](https://gitlab.com/beautifulcanoe/peopleops/orientation) repository.

## Contributing

Please see [CONTRIBUTING.md](/CONTRIBUTING.md) for details on how to contribute to this repository.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
